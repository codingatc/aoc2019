#pragma once

#include <vector>
#include <string_view>
#include <fstream>

std::vector<std::string_view> split(const std::string_view &view, const char *delim)
{
	std::vector<std::string_view> splits;
	std::string::size_type at = 0;
	auto delimLen = strlen(delim);

	do
	{
		auto next = view.find(delim, at);

		std::string_view substr;
		if (next == std::string_view::npos)
		{
			substr = view.substr(at);
			at = next;
		}
		else
		{
			substr = view.substr(at, next - at);
			at = next + delimLen;
		}
		if (substr.empty() == false)
		{
			splits.emplace_back(std::move(substr));
		}
	} while (at != std::string_view::npos);
	return splits;
}

std::vector<std::string_view> split(const char* s, const char* delim)
{
	return split(std::string_view(s), delim);
}

std::string readFile(const char* fileName)
{
	std::ifstream f(fileName);
	return std::string((std::istreambuf_iterator<char>(f)), (std::istreambuf_iterator<char>()));
}