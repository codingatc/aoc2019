// day5.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <string>
#include <vector>
#include <charconv>
#include <cassert>
#include <regex>
#include <fstream>
#include "../../tools.h"

using namespace std;

std::vector<int> getCodes(const char *program)
{
	auto scodes = split(program, ",");

	std::vector<int>codes;
	for (auto scode : scodes)
	{
		int v;
		std::from_chars(scode.data(), scode.data() + scode.size(), v);
		codes.emplace_back(v);
	}
	return codes;
}

struct Computer
{
	Computer(const std::vector<int> &program)
	{
		_codes = program;
	}

	void addInputs(std::initializer_list<int> inputs)
	{
		_inputs.insert(_inputs.end(), inputs.begin(), inputs.end());
	}

	int getOutput()
	{
		int v = *_outputs.begin();
		_outputs.erase(_outputs.begin());
		return v;
	}

	bool halted()
	{
		return _halted;
	}

	void executeUntilOutOfInputs()
	{
		auto inputIt = _inputs.begin();
		for (;;)
		{
			int instrution = _codes[_offset];

			int modes[3] = { (instrution / 100) % 10,
				 (instrution / 1000) % 10,
				(instrution / 10000) % 10 };
			instrution %= 100;

			auto read = [&](int off)
			{
				int a = _codes[_offset + off + 1];
				if (modes[off] == 1)
				{
					return a;
				}
				else
				{
					return _codes[a];
				}
			};

			switch (instrution)
			{
			case 1:
				_codes[_codes[_offset + 3]] = read(0) + read(1);
				_offset += 4;
				break;
			case 2:
				_codes[_codes[_offset + 3]] = read(0) * read(1);
				_offset += 4;
				break;
			case 3:
			{
				if (_inputs.size())
				{
					_codes[_codes[_offset + 1]] = _inputs[0];
					_inputs.erase(_inputs.begin());
					_offset += 2;
				}
				else
				{
					return;
				}
			}
				break;
			case 4:
				_outputs.emplace_back(read(0));
				_offset += 2;
				break;
			case 5:
				if (read(0) != 0)
				{
					_offset = read(1);
				}
				else
				{
					_offset += 3;
				}
				break;
			case 6:
				if (read(0) == 0)
				{
					_offset = read(1);
				}
				else
				{
					_offset += 3;
				}
				break;
			case 7:
				_codes[_codes[_offset + 3]] = (read(0) < read(1)) ? 1 : 0;
				_offset += 4;
				break;
			case 8:
				_codes[_codes[_offset + 3]] = (read(0) == read(1)) ? 1 : 0;
				_offset += 4;
				break;
			case 99:
				_halted = true;
				return;
			default:
				assert(0);
			}
		}
		assert(0);
	}
private:
	std::vector<int> _codes;
	size_t _offset = 0;
	bool _halted = false;
	std::vector<int> _inputs;				// Current inputs.
	std::vector<int> _outputs;
};

int partA(const std::vector<int> &program, int (&settings)[5])
{
	std::unique_ptr<Computer * []> amps = std::make_unique<Computer * []>(5);;
	int signal = 0;
	for (int i = 0; i < 5; i++)
	{
		std::unique_ptr<Computer> amp = std::make_unique<Computer>(program);
		amp->addInputs({ settings[i], signal });
		amp->executeUntilOutOfInputs();
		signal = amp->getOutput();
	}
	return signal;
}

int partA(const std::vector<int>& program)
{
	int seq[] = { 0, 1, 2, 3, 4 };

	int best = -1;
	do
	{
		auto s = partA(program, seq);
		if (s > best)
		{
			best = s;
		}

	} while (next_permutation(seq, seq + 5));
	return best;
}

int partB(const std::vector<int>& program, int(&settings)[5])
{
	std::unique_ptr<Computer * []> amps = std::make_unique<Computer * []>(5);;
	for (int i = 0; i < 5; i++)
	{
		amps[i] = new Computer(program);
		amps[i]->addInputs({ settings[i] });
	}

	int curAmp = 0;
	int signal = 0;
	while (amps[4]->halted() == false)
	{
		amps[curAmp]->addInputs({ signal });
		amps[curAmp]->executeUntilOutOfInputs();
		signal = amps[curAmp]->getOutput();
		curAmp = (curAmp + 1) % 5;
	}
	return signal;
}

int partB(const std::vector<int>& program)
{
	int seq[] = { 5, 6, 7, 8, 9 };

	int best = -1;
	do
	{
		auto s = partB(program, seq);
		if (s > best)
		{
			best = s;
		}

	} while (next_permutation(seq, seq + 5));
	return best;
}

int main()
{
	auto program = getCodes(readFile("problem.txt").c_str());
	int exampleSeq1[5] = { 4,3,2,1,0 };
	int exampleSeq2[5] = { 0,1,2,3,4 };
	int exampleSeq3[5] = { 1,0,4,3,2 };
	int exampleSeq4[5] = { 9,8,7,6,5 };
	int exampleSeq5[5] = { 9,7,8,5,6 };

	printf("Example 1a: %d\n", partA(getCodes("3,15,3,16,1002,16,10,16,1,16,15,15,4,15,99,0,0"), exampleSeq1));
	printf("Example 1b: %d\n", partA(getCodes("3,23,3,24,1002,24,10,24,1002,23,-1,23,101,5,23,23,1,24,23,23,4,23,99,0,0"), exampleSeq2));
	printf("Example 1c: %d\n", partA(getCodes("3,31,3,32,1002,32,10,32,1001,31,-2,31,1007,31,0,33,1002,33,7,33,1,33,31,31,1,32,31,31,4,31,99,0,0,0"), exampleSeq3));

	printf("Part A: %d\n", partA(program));

	printf("Example 2a: %d\n", partB(getCodes("3,26,1001,26,-4,26,3,27,1002,27,2,27,1,27,26,27,4,27,1001,28,-1,28,1005,28,6,99,0,0,5"), exampleSeq4));
	printf("Example 2b: %d\n", partB(getCodes("3,52,1001,52,-5,52,3,53,1,52,56,54,1007,54,5,55,1005,55,26,1001,54,-5,54,1105,1,12,1,53,54,53,1008,54,0,55,1001,55,1,55,2,53,55,53,4,53,1001,56,-1,56,1005,56,6,99,0,0,0,0,10"), exampleSeq5));

	printf("Part B: %d\n", partB(program));
}