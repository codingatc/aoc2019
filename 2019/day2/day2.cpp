// day2.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <string>
#include <vector>
#include <charconv>
#include <cassert>
#include <regex>
#include "../../tools.h"

using namespace std;

const char* example1 = R""(1,9,10,3,2,3,11,0,99,30,40,50)"";
const char* example2 = R""(1,0,0,0,99)"";
const char* example3 = R""(2,3,0,3,99)"";
const char* example4 = R""(2,4,4,5,99,0)"";
const char* example5 = R""(1,1,1,4,99,5,6,0,99)"";

std::vector<int> getCodes(const std::vector<string_view>& scodes)
{
	std::vector<int>codes;
	for (auto scode : scodes)
	{
		int v;
		std::from_chars(scode.data(), scode.data() + scode.size(), v);
		codes.emplace_back(v);
	}
	return codes;
}

int execute(std::vector<int> codes)
{
	for (size_t offset = 0; offset < codes.size(); offset += 4)
	{
		if (codes[offset] == 1)
		{
			int a = codes[offset + 1];
			int b = codes[offset + 2];
			int c = codes[a];
			int d = codes[b];
			codes[codes[offset + 3]] = c + d;
		}
		else if (codes[offset] == 2)
		{
			int a = codes[offset + 1];
			int b = codes[offset + 2];
			int c = codes[a];
			int d = codes[b];
			codes[codes[offset + 3]] = c * d;
		}
		else if (codes[offset] == 99)
		{
			return codes[0];
		}
		else
		{
			assert(0);
		}
	}
	return -1;
}

int execute(const char* program)
{
	return execute(getCodes(split(program, ",")));
}

int partA(std::vector<int> codes, int noun, int verb)
{
	codes[1] = noun;
	codes[2] = verb;
	return execute(codes);
}

int partB(std::vector<int> codes)
{
	for (int i = 0; i < 100; i++)
	{
		for (int j = 0; j < 100; j++)
		{
			if (19690720 == partA(codes, i, j))
			{
				return 100 * i + j;
			}
		}
	}
	return -1;
}

int main()
{
	auto input = readFile("problem.txt");

	printf("Example 1: %d\n", execute(example1));
	printf("Example 2: %d\n", execute(example2));
	printf("Example 3: %d\n", execute(example3));
	printf("Example 4: %d\n", execute(example3));
	printf("Example 5: %d\n", execute(example3));

	auto data = getCodes(split(input, ","));
	printf("Part A: %d\n", partA(data, 12, 2));

	printf("Part B: %d\n", partB(data));
}