// day11.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <charconv>
#include <unordered_map>

#include "../../tools.h"

const char* example1 = R"(10 ORE => 10 A
1 ORE => 1 B
7 A, 1 B => 1 C
7 A, 1 C => 1 D
7 A, 1 D => 1 E
7 A, 1 E => 1 FUEL)";

const char* example2 = R"(9 ORE => 2 A
8 ORE => 3 B
7 ORE => 5 C
3 A, 4 B => 1 AB
5 B, 7 C => 1 BC
4 C, 1 A => 1 CA
2 AB, 3 BC, 4 CA => 1 FUEL)";

const char* example3 = R"(2 VPVL, 7 FWMGM, 2 CXFTF, 11 MNCFX => 1 STKFG
17 NVRVD, 3 JNWZP => 8 VPVL
53 STKFG, 6 MNCFX, 46 VJHF, 81 HVMC, 68 CXFTF, 25 GNMV => 1 FUEL
22 VJHF, 37 MNCFX => 5 FWMGM
139 ORE => 4 NVRVD
144 ORE => 7 JNWZP
5 MNCFX, 7 RFSQX, 2 FWMGM, 2 VPVL, 19 CXFTF => 3 HVMC
5 VJHF, 7 MNCFX, 9 VPVL, 37 CXFTF => 6 GNMV
145 ORE => 6 MNCFX
1 NVRVD => 8 CXFTF
1 VJHF, 6 MNCFX => 4 RFSQX
176 ORE => 6 VJHF)";

const char* example4 = R"(171 ORE => 8 CNZTR
7 ZLQW, 3 BMBT, 9 XCVML, 26 XMNCP, 1 WPTQ, 2 MZWV, 1 RJRHP => 4 PLWSL
114 ORE => 4 BHXH
14 VRPVC => 6 BMBT
6 BHXH, 18 KTJDG, 12 WPTQ, 7 PLWSL, 31 FHTLT, 37 ZDVW => 1 FUEL
6 WPTQ, 2 BMBT, 8 ZLQW, 18 KTJDG, 1 XMNCP, 6 MZWV, 1 RJRHP => 6 FHTLT
15 XDBXC, 2 LTCX, 1 VRPVC => 6 ZLQW
13 WPTQ, 10 LTCX, 3 RJRHP, 14 XMNCP, 2 MZWV, 1 ZLQW => 1 ZDVW
5 BMBT => 4 WPTQ
189 ORE => 9 KTJDG
1 MZWV, 17 XDBXC, 3 XCVML => 2 XMNCP
12 VRPVC, 27 CNZTR => 2 XDBXC
15 KTJDG, 12 BHXH => 5 XCVML
3 BHXH, 2 VRPVC => 7 MZWV
121 ORE => 7 VRPVC
7 XCVML => 6 RJRHP
5 BHXH, 4 VRPVC => 5 LTCX)";

using namespace std;
using Agent = pair<string_view, int64_t>;
using Agents = vector<Agent>;
using Reactions = std::unordered_map<string_view, pair<Agents, int64_t>>;

Reactions getReactions(const string_view &problem)
{
	Reactions reactions;
	for (auto& line : split(problem, "\n"))
	{
		Agents agents;
		bool wasNumber = false;
		bool seenReact = false;
		int64_t pendingNumber;
		for (auto& word : split(line, " "))
		{
			if (word == "=>")
			{
				seenReact = true;
				continue;
			}
			if (wasNumber == false)
			{
				from_chars(word.data(), word.data() + word.size(), pendingNumber);
				wasNumber = true;
			}
			else
			{
				if (word[word.size()- 1] == ',')
					word.remove_suffix(1);
				if (seenReact)
					reactions[word] = make_pair(agents, pendingNumber);
				else
					agents.emplace_back(word, pendingNumber);
				wasNumber = false;
			}
		}
	}
	return reactions;
}

int64_t getOreNeeded(int64_t fuelNeeded, const Reactions &reactions)
{
	std::unordered_map<string_view, int64_t> need{ { "FUEL", fuelNeeded} };
	std::unordered_map<string_view, int64_t> have;

	while (!(need.size() == 1 && need.begin()->first == "ORE"))
	{
		// Pick anything but not the ore.
		auto itor = need.begin();
		if (itor->first == "ORE") itor++;

		// We already have some of it reduce.
		auto haveItor = have.find(itor->first);
		if (haveItor != have.end())
		{
			if (itor->second >= haveItor->second)
			{
				itor->second -= haveItor->second;
				have.erase(haveItor);
			}
			else
			{
				haveItor->second -= itor->second;
				itor->second = 0;
			}
		}
		if (itor->second == 0)
		{
			need.erase(itor);
		}
		else
		{
			// Ok we don't have it. React it.
			auto reactionItor = reactions.find(itor->first);

			// This is the number of times we have to take this.
			int64_t reactionCount = (itor->second + (reactionItor->second.second - 1)) / reactionItor->second.second;
			have[itor->first] += reactionCount * reactionItor->second.second - itor->second;
			if (have[itor->first] == 0)
			{
				have.erase(have.find(itor->first));
			}
			need.erase(itor);

			for (const auto& agent : reactionItor->second.first)
			{
				need[agent.first] += reactionCount * agent.second;
			}
		}
	}
	return need["ORE"];
}

int main()
{
	string input = readFile("problem.txt");

	printf("Example 1: %zd\n", getOreNeeded(1, getReactions(example1)));
	printf("Example 2: %zd\n", getOreNeeded(1, getReactions(example2)));
	printf("Example 3: %zd\n", getOreNeeded(1, getReactions(example3)));
	printf("Example 4: %zd\n", getOreNeeded(1, getReactions(example4)));

	Reactions reactions = getReactions(input.c_str());

	printf("Part A: %zd\n", getOreNeeded(1, reactions));

	// Binary search in two phases. Phase 1. Look upward by power two till we have that much ore.
	int64_t fuelToGet = 1;

	// Due to possible numeric overflow start by scanning in power of 2s up. This takes O(log(n)) time.
	int64_t target = 1'000'000'000'000;
	do
	{
		int64_t result = getOreNeeded(fuelToGet, reactions);
		if (result >= target)
		{
			break;
		}
		fuelToGet *= 2;
	} while (true);

	// Binary search from there, the previous attempt was too small, and the current attempt was to big so its in-between.
	int64_t lb = fuelToGet / 2;
	int64_t ub = fuelToGet;

	int64_t count = ub - lb;
	while (count > 0)
	{
		auto mid = lb + (count / 2);
		int64_t result = getOreNeeded(mid, reactions);

		if (result < target)
		{
			lb = mid + 1;
			count -= count/2 + 1;
		}
		else
		{
			count /= 2;
		}
	}
	// The above is a lower_bound binary search so it will either match exactly or one above.
	if (getOreNeeded(lb, reactions) > target)
		lb--;

	printf("Part B: %zd\n", lb);
}