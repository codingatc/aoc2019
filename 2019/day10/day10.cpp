// day11.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <string>
#include <vector>
#include <charconv>
#include <cassert>
#include <regex>
#include <fstream>
#include <list>
#include "../../tools.h"

static const double pi = 3.14159265358979323846;
static const double epsilon = 1e-10;

const char* example1 = R"(.#..#
.....
#####
....#
...##)";
const char* example2 = R"(......#.#.
#..#.#....
..#######.
.#.#.###..
.#..#.....
..#....#.#
#..#....#.
.##.#..###
##...#..#.
.#....####)";
const char* example3 = R"(#.#...#.#.
.###....#.
.#....#...
##.#.#.#.#
....#.#.#.
.##..###.#
..#...##..
..##....##
......#...
.####.###.)";
const char* example4 = R"(.#..#..###
####.###.#
....###.#.
..###.##.#
##.##.#.#.
....###..#
..#.#..#.#
#..#.#.###
.##...##.#
.....#.#..)";
const char* example5 = R"(.#..##.###...#######
##.############..##.
.#.######.########.#
.###.#######.####.#.
#####.##.#.##.###.##
..#####..#.#########
####################
#.####....###.#.#.##
##.#################
#####.##.###..####..
..######..##.#######
####.##.####...##..#
.#####..#.######.###
##...#.##########...
#.##########.#######
.####.#.###.###.#.##
....##.##.###..#####
.#.#.###########.###
#.#.#.#####.####.###
###.##.####.##.#..##)";

using namespace std;

template <class T>
class Grid
{
public:
	Grid(int width, int height) : width(width), height(height)
	{
		buffer = std::make_unique<T[]>(width * height);
		for (int x = 0; x < width; x++)
		{
			for (int y = 0; y < height; y++)
			{
				set(x, y, T{});
			}
		}
	}

	int getWidth() const
	{
		return width;
	}

	int getHeight() const
	{
		return height;
	}

	void set(int x, int y, T c)
	{
		assert(x < width);
		assert(y < height);
		buffer[y * width + x] = c;
	}
	T get(int x, int y) const
	{
		assert(x < width);
		assert(y < height);
		return buffer[y * width + x];
	}

private:
	int width, height;
	std::unique_ptr<T[]> buffer;
};

Grid<char> loadGrid(const char* grid)
{
	auto lines = split(grid, "\n");
	auto height = (int)lines.size();
	auto width = (int)lines[0].size();
	Grid<char> g(width, height);

	for (int x = 0; x < width; x++)
	{
		for (int y = 0; y < height; y++)
		{
			g.set(x, y, lines[y][x]);
		}
	}
	return g;
}

struct Asteroid
{
	int x, y;
	double angle;
	int distSquared;
};

std::list<Asteroid> getAsteroids(const Grid<char>& grid, int cx, int cy)
{
	std::list<Asteroid> asteroids;

	for (int y = 0; y < grid.getHeight(); y++)
	{
		for (int x = 0; x < grid.getWidth(); x++)
		{
			if (grid.get(x, y) == '#' && !(x == cx && y == cy))
			{
				double angle = std::atan2(cy - y, cx - x);
				angle -= pi / 2.0;
				if (angle < -epsilon) angle += 2.0 * pi;
				int distSquared = (x - cx) * (x - cx) + (y - cy) * (y - cy);
				asteroids.emplace_back(Asteroid{ x, y, angle, distSquared });
			}
		}
	}

	// List though it is a linked list has a way to sort in O(n log n)
	asteroids.sort([](const Asteroid& a, const Asteroid& b)
		{
			if (std::abs(a.angle - b.angle) < epsilon)
			{
				return a.distSquared < b.distSquared;
			}
			else
			{
				return a.angle < b.angle;
			}
		});
	return asteroids;
}

std::tuple<int, int, int> partA(const Grid<char>& grid)
{
	int bestx = 0, besty = 0, best = -1;

	for (int y = 0; y < grid.getHeight(); y++)
	{
		for (int x = 0; x < grid.getWidth(); x++)
		{
			if (grid.get(x, y) == '#')
			{
				auto asteroids = getAsteroids(grid, x, y);

				// Loop through each one recalling the last angle, when it moves you get another hit.
				int canSee = 0;
				double lastAngle = std::numeric_limits<double>::max();			// Unreasonable.

				for (auto& asteroid : asteroids)
				{
					if (abs(asteroid.angle - lastAngle) < epsilon) continue;
					canSee++;
					lastAngle = asteroid.angle;
				}
				if (canSee > best)
				{
					best = canSee;
					bestx = x;
					besty = y;
				}
			}
		}
	}

	return std::make_tuple(bestx, besty, best);
}

std::vector<std::pair<int, int>> partB(const Grid<char>& grid, int x, int y)
{
	auto asteroids = getAsteroids(grid, x, y);
	std::vector<std::pair<int, int>> removed;
	while (asteroids.empty() == false)
	{
		double lastAngle = std::numeric_limits<double>::max();			// Unreasonable.
		auto itor = asteroids.begin();

		while (itor != asteroids.end())
		{
			if (abs(itor->angle - lastAngle) < epsilon)
			{
				++itor;
			}
			else
			{
				removed.emplace_back(itor->x, itor->y);

				lastAngle = itor->angle;
				itor = asteroids.erase(itor);
			}
		}
	}
	return removed;
}


int main()
{
	auto input = readFile("problem.txt");

	{
		const char* examples[] = { example1,example2,example3, example4,example5 };
		for (int i = 0; i < sizeof(examples) / sizeof(examples[0]); i++)
		{
			auto grid = loadGrid(examples[i]);
			auto resultPartA = partA(grid);
			int bestx = get<0>(resultPartA);
			int besty = get<1>(resultPartA);
			int best = get<2>(resultPartA);
			printf("Example %d: %d (%d,%d)\n", i + 1, best, bestx, besty);
		}
	}
	{
		auto grid = loadGrid(example5);
		auto resultPartA = partA(grid);
		int bestx = get<0>(resultPartA);
		int besty = get<1>(resultPartA);
		int best = get<2>(resultPartA);
		auto removed = partB(grid, bestx, besty);

		int toSee[] = { 1, 2, 3, 10, 20, 50, 100, 199, 200, 201, 299 };
		for (int i : toSee)
		{
			printf("The %d asteroid to be vaporized is at %d,%d\n", i, removed[i - 1].first, removed[i - 1].second);
		}
	}
	{
		auto grid = loadGrid(input.c_str());

		auto resultPartA = partA(grid);
		int bestx = get<0>(resultPartA);
		int besty = get<1>(resultPartA);
		int best = get<2>(resultPartA);
		printf("Part A: %d (%d,%d)\n", best, bestx, besty);

		auto removed = partB(grid, bestx, besty);

		printf("Part B: %d\n", removed[199].first * 100 + removed[199].second);

	}
}