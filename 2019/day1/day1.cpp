// day1.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <string>
#include <vector>
#include <charconv>
#include <regex>
#include "../../tools.h"

using namespace std;

const char *example1 = R""(
12
14
1969
100756
)"";

int partA(int mass)
{
	return mass / 3 - 2;
}

int partA(const string_view& line)
{
	int mass;
	from_chars(line.data(), line.data() + line.size(), mass);
	return partA(mass);
}

int partB(const string_view& line)
{
	int mass;
	int fuel = 0;
	from_chars(line.data(), line.data() + line.size(), mass);

	for(;;)
	{
		int ft = partA(mass);
		if (ft <= 0)
			return fuel;
		fuel += ft;
		mass = ft;
	}
}

int main()
{
	auto input = readFile("problem.txt");
	auto e1 = split(example1, "\n");

	for (const auto& str : e1)
	{
		printf("%d\n", partA(str));
	}

	auto data = split(input.c_str(), "\n");

	int sum = 0;
	for (const auto& str : data)
	{
		sum += partA(str);
	}

	printf("PartA: %d\n", sum);

	sum = 0;
	for (const auto& str : data)
	{
		sum += partB(str);
	}
	printf("PartB: %d\n", sum);
}