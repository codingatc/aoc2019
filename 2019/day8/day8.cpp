// day8.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <string>
#include <vector>
#include <charconv>
#include <cassert>
#include <regex>
#include <fstream>
#include <string_view>
#include "../../tools.h"
#include "../../graph.h"

using namespace std;

vector<string_view> makeLayers(const string_view &input, int v)
{
	vector<string_view> s;
	for (size_t i = 0; i < input.size(); i += v)
	{
		s.emplace_back(input.data() + i, v);
	}
	return s;
}

int count(const string_view &v, char c)
{
	string_view::size_type at = -1;
	auto cnt = 0;

	do
	{
		at++;
		at = v.find(c, at);
		if (at != v.npos) cnt++;
	} while (at != v.npos);
	return cnt;
}

int main()
{
	auto input = readFile("problem.txt");
	//auto input = "0222112222120000";
	auto codes = makeLayers(input, 25*6);
	
	int best = std::numeric_limits<int>::max();
	int bestIdx = 0;
	
	for (auto i = 0; i < codes.size(); i++)
	{
		auto& code = codes[i];
		int k = count(code, '0');
		if (k < best)
		{
			best = k;
			bestIdx = i;
		}
	}

	printf("Part A: %d\n", count(codes[bestIdx], '1') * count(codes[bestIdx], '2'));
	
	printf("Part B:\n");
	auto fill = string(codes[0]);
	
	for (auto i = 1; i < codes.size(); i++)
	{
		auto& code = codes[i];
	
		for (auto j = 0; j < code.size(); j++)
		{
			if (fill[j] == '2') fill[j] = code[j];
		}
	}

	for (auto& c : fill)
	{
		if (c == '1') c = '*';
		if (c == '0') c = ' ';
	}
	for (int i = 0; i < 6; i++)
	{
		printf("%s\n", std::string(fill.begin() + 25*i, fill.begin() + 25 * (i+1)).c_str());
	}
}