// day6.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
#include "../../tools.h"
#include "../../graph.h"

using namespace std;

const char* example1 = R""(COM)B
B)C
C)D
D)E
E)F
B)G
G)H
D)I
E)J
J)K
K)L)"";

const char* example2 = R""(COM)B
B)C
C)D
D)E
E)F
B)G
G)H
D)I
E)J
J)K
K)L
K)YOU
I)SAN)"";

int partA(const char* problem)
{
	DirectedGraph<string_view> graph;
	for (auto a : split(problem, "\n"))
	{
		auto v = split(a, ")");
		graph.add(v[0], v[1]);
	}
	auto reach = reachability(graph.reverse());

	size_t sum = 0;
	for (const auto& node : graph.nodes())
	{
		sum += reach[node];
	}
	return (int)sum;
}

int partB(const char *problem)
{
	Graph<string_view> graph;
	auto splited = split(problem, "\n");
	for (auto a : splited)
	{
		auto v = split(a, ")");
		graph.add(v[0], v[1]);
	}
	return bfs(graph, string_view("YOU")).find("SAN")->second - 2;
}

int main()
{
	auto input = readFile("problem.txt");
	
	printf("Example1: %d\n", partA(example1));
	printf("Part A: %d\n", partA(input.c_str()));

	printf("Example 2: %d\n", partB(example2));
	printf("Part B: %d\n", partB(input.c_str()));
}