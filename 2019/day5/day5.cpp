// day5.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <string>
#include <vector>
#include <charconv>
#include <cassert>
#include <regex>
#include "../../tools.h"

using namespace std;

std::vector<int> getCodes(const std::vector<string_view>& scodes)
{
	std::vector<int>codes;
	for (auto scode : scodes)
	{
		int v;
		std::from_chars(scode.data(), scode.data() + scode.size(), v);
		codes.emplace_back(v);
	}
	return codes;
}

std::vector<int> execute(const char *program, std::vector<int> inputs)
{
	std::vector<int> codes = getCodes(split(program, ","));
	std::vector<int> outputs;
	auto inputIt = inputs.begin();
	size_t offset = 0;
	for(;;)
	{
		int instrution = codes[offset];

		int modes[3] = { (instrution / 100) % 10,
			 (instrution / 1000) % 10,
			(instrution / 10000) % 10 };
		instrution %= 100;

		auto read = [&](int off)
		{
			int a = codes[offset + off + 1];
			if (modes[off] == 1)
			{
				return a;
			}
			else
			{
				return codes[a];
			}
		};

		switch (instrution)
		{
		case 1:
			codes[codes[offset + 3]] = read(0) + read(1);
			offset += 4;
			break;
		case 2:
			codes[codes[offset + 3]] = read(0) * read(1);
			offset += 4;
			break;
		case 3:
			codes[codes[offset + 1]] = (*inputIt);
			inputIt++;
			offset+=2;
			break;
		case 4:
			outputs.emplace_back(read(0));
			offset+=2;
			break;
		case 5:
			if (read(0) != 0)
			{
				offset = read(1);
			}
			else
			{
				offset += 3;
			}
			break;
		case 6:
			if (read(0) == 0)
			{
				offset = read(1);
			}
			else
			{
				offset += 3;
			}
			break;
		case 7:
			codes[codes[offset + 3]] = (read(0) < read(1)) ? 1 : 0;
			offset += 4;
			break;
		case 8:
			codes[codes[offset + 3]] = (read(0) == read(1)) ? 1 : 0;
			offset += 4;
			break;
		case 99:
			return outputs;
		default:
			assert(0);
		}
	}
	assert(0);
}

int main()
{
	auto input = readFile("problem.txt");

	printf("Example 1a: %d\n", execute("3,0,4,0,99", { 1 }).back());
	printf("Part a: %d\n", execute(input.c_str(), { 1 }).back());

	printf("Example 2a(0): %d\n", execute("3,9,8,9,10,9,4,9,99,-1,8", { 8 }).back());
	printf("Example 2a(1): %d\n", execute("3,9,8,9,10,9,4,9,99,-1,8", { 1 }).back());

	printf("Example 2b(0): %d\n", execute("3,9,7,9,10,9,4,9,99,-1,8", { 7 }).back());
	printf("Example 2b(1): %d\n", execute("3,9,7,9,10,9,4,9,99,-1,8", { 8 }).back());
	printf("Example 2b(2): %d\n", execute("3,9,7,9,10,9,4,9,99,-1,8", { 9 }).back());

	printf("Example 2c(0): %d\n", execute("3,3,1108,-1,8,3,4,3,99", { 8 }).back());
	printf("Example 2c(1): %d\n", execute("3,3,1108,-1,8,3,4,3,99", { 1 }).back());

	printf("Example 2d(0): %d\n", execute("3,3,1107,-1,8,3,4,3,99", { 7 }).back());
	printf("Example 2d(1): %d\n", execute("3,3,1107,-1,8,3,4,3,99", { 8 }).back());
	printf("Example 2d(2): %d\n", execute("3,3,1107,-1,8,3,4,3,99", { 9 }).back());

	printf("Example 2e(0): %d\n", execute("3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9", { 0 }).back());
	printf("Example 2e(1): %d\n", execute("3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9", { 1 }).back());

	printf("Example 2f(0): %d\n", execute("3,3,1105,-1,9,1101,0,0,12,4,12,99,1", { 0 }).back());
	printf("Example 2f(1): %d\n", execute("3,3,1105,-1,9,1101,0,0,12,4,12,99,1", { 1 }).back());

	auto large = "3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99";
	printf("Example 2g(0): %d\n", execute(large, { 0 }).back());
	printf("Example 2g(1): %d\n", execute(large, { 8 }).back());
	printf("Example 2g(2): %d\n", execute(large, { 16 }).back());

	printf("Part b: %d\n", execute(input.c_str(), { 5 }).back());
}