// day3.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <string>
#include <vector>
#include <charconv>
#include <cassert>
#include <regex>
#include <queue>
#include <set>
#include <map>
#include <algorithm>
#include <iterator>
#include "../../tools.h"

using namespace std;

const char* example1 = R""(R8,U5,L5,D3
U7,R6,D4,L4)"";

const char* example2 = R""(R75,D30,R83,U83,L12,D49,R71,U7,L72
U62,R66,U55,R34,D71,R55,D58,R83)"";
const char* example3 = R""(R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51
U98,R91,D20,R16,D67,R40,U7,R15,U6,R7)"";

using loc = pair<int, int>;
struct Code
{
	loc direction;
	int amount;
};

std::vector<Code> getCodes(const std::vector<string_view>& scodes)
{
	std::vector<Code> codes;
	for (auto scode : scodes)
	{
		int amount;
		loc direction;
		switch (scode[0])
		{
		default:
			assert(0);
		case 'R':
			direction = loc{ 1, 0 };
			break;
		case 'L':
			direction = loc{ -1, 0 };
			break;
		case 'U':
			direction = loc{ 0, 1 };
			break;
		case 'D':
			direction = loc{ 0, -1 };
			break;
		}

		std::from_chars(scode.data() + 1, scode.data() + scode.size(), amount);
		codes.emplace_back(Code{ direction, amount });
	}
	return codes;
}


std::set<loc> executeA(const std::vector<Code>& codes)
{
	loc cursor{ 0, 0 };
	std::set<loc> out;
	for (auto code : codes)
	{
		for (int i = 0; i < code.amount; i++)
		{
			cursor.first += code.direction.first;
			cursor.second += code.direction.second;
			// Exploit the fact that new cells = 0;
			out.emplace(cursor);
		}
	}
	return out;
}

std::map<loc, int> executeB(const std::vector<Code>& codes)
{
	loc cursor{ 0, 0 };
	std::map<loc, int> out;
	int counter = 0;
	for (auto code : codes)
	{
		for (int i = 0; i < code.amount; i++)
		{
			cursor.first += code.direction.first;
			cursor.second += code.direction.second;
			// Exploit the fact that new cells = 0;
			
			counter++;
			if (out[cursor] == 0)
			{
				out[cursor] = counter;
			}
		}
	}
	return out;
}

int partA(const char* problem)
{
	auto lines = split(problem, "\n");

	set<loc> setsa = executeA(getCodes(split(lines[0], ",")));
	set<loc> setsb = executeA(getCodes(split(lines[1], ",")));
	set<loc> out;

	set_intersection(setsa.begin(), setsa.end(), setsb.begin(), setsb.end(), std::inserter(out, out.end()));

	int minDist = std::numeric_limits<int>::max();

	for (auto loc : out)
	{
		int dist = abs(loc.first) + abs(loc.second);
		if (dist < minDist)
			minDist = dist;
	}
	return minDist;

}

set<loc> mapToSet(const map<loc, int>& map)
{
	set<loc> set;
	for (auto item : map)
	{
		set.emplace(item.first);
	}
	return set;
}

int partB(const char* problem)
{
	auto lines = split(problem, "\n");

	auto mapsa = executeB(getCodes(split(lines[0], ",")));
	auto mapsb = executeB(getCodes(split(lines[1], ",")));

	auto setsa = mapToSet(mapsa);
	auto setsb = mapToSet(mapsb);
	set<loc> out;

	set_intersection(setsa.begin(), setsa.end(), setsb.begin(), setsb.end(), std::inserter(out, out.end()));

	int minDist = std::numeric_limits<int>::max();

	for (auto loc : out)
	{
		int dist = mapsa.at(loc) + mapsb.at(loc);
		if (dist < minDist)
		{
			minDist = dist;
		}
	}
	return minDist;

}

int main()
{
	auto input = readFile("problem.txt");

	printf("Example 1a: %d\n", partA(example1));
	printf("Example 2a: %d\n", partA(example2));
	printf("Example 3a: %d\n", partA(example3));
	printf("Part a: %d\n", partA(input.c_str()));

	printf("Example 1b: %d\n", partB(example1));
	printf("Example 2b: %d\n", partB(example2));
	printf("Example 3b: %d\n", partB(example3));
	printf("Part b: %d\n", partB(input.c_str()));
}