// day5.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <string>
#include <vector>
#include <charconv>
#include <cassert>
#include <regex>
#include <fstream>
#include "../../tools.h"

using namespace std;

std::vector<int64_t> getCodes(const char* program)
{
	auto scodes = split(program, ",");

	std::vector<int64_t>codes;
	for (auto scode : scodes)
	{
		int64_t v;
		std::from_chars(scode.data(), scode.data() + scode.size(), v);
		codes.emplace_back(v);
	}
	return codes;
}

struct Computer
{
	Computer(const char *program)
	{
		_codes = getCodes(program);
	}

	Computer(const std::vector<int64_t>& program)
	{
		_codes = program;
	}

	void addInputs(std::initializer_list<int> inputs)
	{
		_inputs.insert(_inputs.end(), inputs.begin(), inputs.end());
	}

	bool getOutput(int64_t&x)
	{
		if (_outputs.empty())
			return false;
		x = *_outputs.begin();
		_outputs.erase(_outputs.begin());
		return true;
	}

	bool halted()
	{
		return _halted;
	}

	void executeUntilOutOfInputs()
	{
		auto inputIt = _inputs.begin();
		for (;;)
		{
			_instructionsRan++;
			int64_t instrution = _codes[_offset];

			int64_t modes[3] = { (instrution / 100) % 10,
				 (instrution / 1000) % 10,
				(instrution / 10000) % 10 };
			instrution %= 100;

			auto readMode = [&](int off)
			{
				if (modes[off] == 1)
				{
					return  read(_offset + off + 1);
				}
				else if (modes[off] == 2)
				{
					return read(read(_offset + off + 1) + _relBase);
				}
				else
				{
					return read(read(_offset + off + 1));
				}
			};
			auto writeMode = [&](int off, int64_t value)
			{
				if (modes[off] == 1)
				{
					assert(0);
				}
				else if (modes[off] == 2)
				{
					return write(read(_offset + off + 1) + _relBase, value);
				}
				else
				{
					return write(read(_offset + off + 1), value);
				}
			};

			switch (instrution)
			{
			case 1:
				writeMode(2, readMode(0) + readMode(1));
				_offset += 4;
				break;
			case 2:
				writeMode(2, readMode(0) * readMode(1));
				_offset += 4;
				break;
			case 3:
			{
				if (_inputs.size())
				{
					writeMode(0, _inputs[0]);
					_inputs.erase(_inputs.begin());
					_offset += 2;
				}
				else
				{
					return;
				}
			}
			break;
			case 4:
				_outputs.emplace_back(readMode(0));
				_offset += 2;
				break;
			case 5:
				if (readMode(0) != 0)
				{
					_offset = readMode(1);
				}
				else
				{
					_offset += 3;
				}
				break;
			case 6:
				if (readMode(0) == 0)
				{
					_offset = readMode(1);
				}
				else
				{
					_offset += 3;
				}
				break;
			case 7:
				writeMode(2, (readMode(0) < readMode(1)) ? 1 : 0);
				_offset += 4;
				break;
			case 8:
				writeMode(2, (readMode(0) == readMode(1)) ? 1 : 0);
				_offset += 4;
				break;
			case 9:
				_relBase += readMode(0);
				_offset += 2;
				break;
			case 99:
				_halted = true;
				return;
			default:
				assert(0);
			}
		}
		assert(0);
	}
private:
	int64_t read(int64_t at)
	{
		expand(at);
		return _codes[at];
	}
	void write(int64_t at, int64_t v)
	{
		expand(at);
		_codes[at] = v;
	}
	void expand(int64_t at)
	{
		assert(at >= 0);
		if ((size_t)at >= _codes.size())
		{
			_codes.resize(at + 1);
		}
	}

	int64_t _instructionsRan = 0;
	int64_t _relBase = 0;
	std::vector<int64_t> _codes;
	size_t _offset = 0;
	bool _halted = false;
	std::vector<int64_t> _inputs;				// Current inputs.
	std::vector<int64_t> _outputs;
};


int main()
{
	auto program = getCodes(readFile("problem.txt").c_str());

	const char* example1 = "109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99";
	const char* example2 = "1102,34915192,34915192,7,4,7,99,0";
	const char* example3 = "104,1125899906842624,99";

	{
		printf("Example 1: ");
		Computer c(example1);
		c.executeUntilOutOfInputs();
		bool first = true;
		int64_t x;
		while (c.getOutput(x))
		{
			if (!first) printf(",");
			first = false;
			printf("%lld", x);
		}
		printf("\n");
	}
	{
		Computer c(example2);
		c.executeUntilOutOfInputs();
		int64_t x;
		c.getOutput(x);
		printf("Example 2: %lld\n", x);
	}
	{
		Computer c(example3);
		c.executeUntilOutOfInputs();
		int64_t x;
		c.getOutput(x);
		printf("Example 3: %lld\n", x);
	}
	printf("\n");
	{
		Computer c(program);
		c.addInputs({ 1 });
		c.executeUntilOutOfInputs();
		int64_t x;
		c.getOutput(x);

		printf("Part A: %lld\n", x);
	}
	{
		Computer c(program);
		c.addInputs({ 2 });
		c.executeUntilOutOfInputs();
		int64_t x;
		c.getOutput(x);

		printf("Part B: %lld\n", x);
	}
}