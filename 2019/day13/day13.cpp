// day11.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <algorithm>
#include <iostream>
#include <string>
#include <vector>
#include <charconv>
#include <cassert>
#include <regex>
#include <fstream>
#include <map>
#include <unordered_map>
#include <thread>
#define NOMINMAX
#include <Windows.h>

#include "../../tools.h"
#include "../../graph.h"

using namespace std;

std::vector<int64_t> getCodes(const char* program)
{
	auto scodes = split(program, ",");

	std::vector<int64_t>codes;
	for (auto scode : scodes)
	{
		int64_t v;
		std::from_chars(scode.data(), scode.data() + scode.size(), v);
		codes.emplace_back(v);
	}
	return codes;
}

struct Computer
{
	Computer(const char* program)
	{
		_codes = getCodes(program);
	}

	Computer(const std::vector<int64_t>& program)
	{
		_codes = program;
	}

	void addInputs(std::initializer_list<int64_t> inputs)
	{
		_inputs.insert(_inputs.end(), inputs.begin(), inputs.end());
	}

	bool hasOutputs()
	{
		return _outputs.size();
	}

	bool getOutput(int64_t& x)
	{
		if (_outputs.empty())
			return false;
		x = *_outputs.begin();
		_outputs.erase(_outputs.begin());
		return true;
	}

	bool halted()
	{
		return _halted;
	}

	void executeUntilOutOfInputs()
	{
		auto inputIt = _inputs.begin();
		for (;;)
		{
			_instructionsRan++;
			int64_t instrution = _codes[_offset];

			int64_t modes[3] = { (instrution / 100) % 10,
				 (instrution / 1000) % 10,
				(instrution / 10000) % 10 };
			instrution %= 100;

			auto readMode = [&](int off)
			{
				if (modes[off] == 1)
				{
					return  read(_offset + off + 1);
				}
				else if (modes[off] == 2)
				{
					return read(read(_offset + off + 1) + _relBase);
				}
				else
				{
					return read(read(_offset + off + 1));
				}
			};
			auto writeMode = [&](int off, int64_t value)
			{
				if (modes[off] == 1)
				{
					assert(0);
				}
				else if (modes[off] == 2)
				{
					return write(read(_offset + off + 1) + _relBase, value);
				}
				else
				{
					return write(read(_offset + off + 1), value);
				}
			};

			switch (instrution)
			{
			case 1:
				writeMode(2, readMode(0) + readMode(1));
				_offset += 4;
				break;
			case 2:
				writeMode(2, readMode(0) * readMode(1));
				_offset += 4;
				break;
			case 3:
			{
				if (_inputs.size())
				{
					writeMode(0, _inputs[0]);
					_inputs.erase(_inputs.begin());
					_offset += 2;
				}
				else
				{
					return;
				}
			}
			break;
			case 4:
				_outputs.emplace_back(readMode(0));
				_offset += 2;
				break;
			case 5:
				if (readMode(0) != 0)
				{
					_offset = readMode(1);
				}
				else
				{
					_offset += 3;
				}
				break;
			case 6:
				if (readMode(0) == 0)
				{
					_offset = readMode(1);
				}
				else
				{
					_offset += 3;
				}
				break;
			case 7:
				writeMode(2, (readMode(0) < readMode(1)) ? 1 : 0);
				_offset += 4;
				break;
			case 8:
				writeMode(2, (readMode(0) == readMode(1)) ? 1 : 0);
				_offset += 4;
				break;
			case 9:
				_relBase += readMode(0);
				_offset += 2;
				break;
			case 99:
				_halted = true;
				return;
			default:
				assert(0);
			}
		}
		assert(0);
	}
private:
	int64_t read(int64_t at)
	{
		expand(at);
		return _codes[at];
	}
	void write(int64_t at, int64_t v)
	{
		expand(at);
		_codes[at] = v;
	}
	void expand(int64_t at)
	{
		assert(at >= 0);
		if ((size_t)at >= _codes.size())
		{
			_codes.resize(at + 1);
		}
	}

	int64_t _instructionsRan = 0;
	int64_t _relBase = 0;
	std::vector<int64_t> _codes;
	size_t _offset = 0;
	bool _halted = false;
	std::vector<int64_t> _inputs;				// Current inputs.
	std::vector<int64_t> _outputs;
};

using Display = std::map<std::pair<int64_t, int64_t>, int64_t>;

struct State
{
	Computer c;
	Display d;

	std::pair<int64_t, int64_t> getBallPos()
	{
		for (auto i : d)
		{
			if (i.second == 4)
				return i.first;
		}
	}
	std::pair<int64_t, int64_t> getPaddlePos()
	{
		for (auto i : d)
		{
			if (i.second == 3)
				return i.first;
		}
	}
	int64_t getScore()
	{
		auto itor = d.find(make_pair<int64_t, int64_t>(-1, 0));
		if (itor != d.end())
			return itor->second;
		return 0;
	}
	bool playing()
	{
		for (auto i : d)
		{
			if (i.second == 4)
				return true;
		}
		return false;
	}
	int bricks()
	{
		int bricks = 0;
		for (auto i : d)
		{
			if (i.second == 2)
				bricks++;
		}
		return bricks;
	}
	int64_t pri()
	{
		int64_t xBall = 0;
		int64_t xPaddle = 0;
		for (auto i : d)
		{
			if (i.second == 4)
				xBall = i.first.first;
			if (i.second == 3)
				xPaddle = i.first.first;
		}
		return abs(xBall - xPaddle);
	}
};

// Good for debugging
void setxy(int x, int y)
{
	COORD p = { (SHORT)x, (SHORT)y };
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), p);
}
char getChar(int x, int y, const Display& display)
{
	auto itor = display.find(make_pair(x, y));
	if (itor == display.end()) return ' ';
	switch (itor->second)
	{
	case 0: return ' ';
	case 1:
		if (y == 0)
		{
			if (x == 0 || display.find(make_pair(x + 1, y)) == display.end())
			{
				return '+';
			}
			return '-';
		}
		return '|';
	case 2: return '#';
	case 3: return '_';
	case 4: return '*';
	}
}

void printDisplay(const Display& display, const Display& old)
{
	int64_t maxx = 0;
	int64_t maxy = 0;
	for (auto& tile : display)
	{
		if (tile.first.first < 0 || tile.first.second < 0) continue;
		maxx = std::max(maxx, tile.first.first);
		maxy = std::max(maxy, tile.first.second);
	}
	char map[] = { ' ', '|', '#', '_', '*' };
	for (int64_t y = 0; y <= maxy; y++)
	{
		for (int64_t x = 0; x <= maxx; x++)
		{
			char newc = getChar(x, y, display);
			char oldc = getChar(x, y, old);
			if (oldc != newc)
			{
				setxy((int)x, (int)y);
				printf("%c", newc);
			}
		}
	}
	setxy(0, (int)maxy + 1);
	auto itor = display.find(make_pair<int64_t, int64_t>(-1, 0));
	
	auto s = (itor == display.end()) ? 0 : itor->second;
	printf("Score: %zd\n", s);
}

void exec(State& s)
{
	s.c.executeUntilOutOfInputs();
	int64_t x, y, tile;
	while (s.c.hasOutputs())
	{
		s.c.getOutput(x);
		s.c.getOutput(y);
		s.c.getOutput(tile);
		s.d[make_pair(x, y)] = tile;
	}
}

void playRound(State &s, int64_t joystick)
{
	s.c.addInputs({ joystick });
	exec(s);
}

#if 0
// Break one brick that ends this scan
bool scan(State &initState)
{
	using QueueEntry = std::pair<std::shared_ptr <State>, int64_t>;
	struct Comparer
	{
		bool operator()(const QueueEntry& a, QueueEntry& b) const
		{
			return a.second > b.second;
		}
	};
	auto pri = [](State& s)
	{
		return s.pri();
	};
	using queue = std::priority_queue<QueueEntry, std::vector<QueueEntry>, Comparer>;
	queue frontier;
	auto s = std::make_shared<State>(initState);
	int bricks = s->bricks();
	auto initPri = pri(*s);
	frontier.emplace(std::move(s), initPri);

	while (frontier.empty() == false)
	{
		QueueEntry item = frontier.top();
		frontier.pop();


		if (bricks != item.first->bricks())
		{
			initState = *item.first;
			return true;
		}


		for (auto& child : { -1, 0, 1 })
		{
			auto c3 = std::make_shared<State>(*item.first);
			int64_t py = c3->getBally();
			playRound(*c3, child);
			if (item.first->c.halted() == true || item.first->playing() == false)
			{
				continue;
			}
			int64_t py2 = c3->getBally();
			if (abs(py - py2) == 1)
			{
				auto thisPri = pri(*c3);
				frontier.emplace(std::move(c3), thisPri);
			}
			else
			{
				int x = 0;
			}
		}
	}
	return false;
}
#endif

int main()
{
	auto program = getCodes(readFile("problem.txt").c_str());
	program[0] = 2; // Freeplay

	Display display;

	Computer c(program);
	State s = { c, Display() };
	exec(s);
	int partA = s.bricks();
	auto ballPos = s.getBallPos();

	int x = 0;
	int lastScore = 0;
	std::vector<int> inputs;
	Display old;
	while (s.bricks() != 0)
	{
		printDisplay(s.d, old);
		old = s.d;

		// Predict where the ball is going to go and move the paddle tword it.
		int64_t dx = s.getBallPos().first - s.getPaddlePos().first;
		if (dx)
			dx /= abs(dx);
		playRound(s, dx);
		inputs.emplace_back((int)dx);
		std::this_thread::sleep_for(std::chrono::milliseconds(50));
	}
	printDisplay(s.d, old);
	printf("Part A: %d\n", partA);
	printf("Part B: %zd\n", s.getScore());
}