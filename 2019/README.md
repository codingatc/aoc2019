When creating the code for AOC I do two passes:

1. Super fast hacky code to try to place.
2. I then cleanup the code into something more akin to what I would use in production.